#!/bin/bash
virsh list --name --all|head -n-1 | while read n 
do 
  [[ ! -z $n ]] && virsh dominfo  $n
  [[ ! -z $n ]] && virsh dommemstat $n|head -1|awk '{print "Actual Memory="$2/1024/1024" GB"}'
  echo "CPU INFO"
  [[ ! -z $n ]] && virsh vcpuinfo  $n |grep -w "CPU:" 
  # "CPU PIN"
  [[ ! -z $n ]] && virsh vcpupin  $n
  echo "Block Device"
  virsh domstats $n|awk -F'=' '/block\.[0-9]\.physical|block\.[0-9]\.name|block\.[0-9]\.path/ { if ($2 ~ /^[:0-9:]/) print $1"="$2/1024/1024/1024"GB"; else print $0;}'
  echo "Network"
  virsh domiflist $n
  echo "##################################"
done

virsh list --name --all|head -n-1 | (while read n 
do 
a=`virsh vcpuinfo $n|grep -w "CPU:"| sed 's/.*\(..\)/\1/'|paste -sd","`
a+=","
b+=$a
done
c=`awk -F',' '{print NF-2}' <<< $b`
echo "VM Current ($c) Physical CPU: CPU affinity $b")

virsh list --name --all|head -n-1 | (while read n 
do 
a=`virsh dommemstat $n |head -n1|awk -F' ' '{ print $2/1024/1024 }'`
b=`echo $a  $b | awk '{print $1 + $2}'`
done
echo "VM Total Memory $b GB")


virsh list --name --all|head -n-1 | (while read n 
do 
a=`virsh domstats $n|awk -F'=' '/block\.[0-9]\.physical/ {sum += $2} END { print sum/1024/1024/1024 }'`
b=`echo $a  $b | awk '{print $1 + $2}'`
done
echo "VM Total Storage $b GB")
