#!/bin/bash

WIKIURL=https://wiki.mnfgroup.limited/rest/api/content/

read -p 'SSH Username: ' UNAME
read -sp 'SSH Password: ' UPASS
read -p 'HOST Server IP: ' HOSTIP
read -p 'Wiki Username: ' WIKIUSR
read -sp 'Wiki Password: ' WIKIPASS
read -p 'Wiki Page ID: ' PARENTPAGE
read -p 'Wiki Space: ' SPACE

#Manual Input
#UNAME=
#UPASS=
#WIKIUSR=
#WIKIPASS=
#HOSTIP=
#PARENTPAGE=
#SPACE=

#Command tracing
exec 2>>myfile.log
set -x

HOSTRESULT=$(
/usr/bin/expect << EOF
log_file myfile.log
#log_user 0
set timeout 10
spawn ssh -o StrictHostKeyChecking=no $UNAME@$HOSTIP
expect {
	 timeout {
	 	send_user "ErrExpt:Connection timed out"
		exit 1
	}
	"Connection refused" {
		send_user "ErrExpt:Connection refused"
		exit 1
	}
	"*sword*" { 
		send "$UPASS\r" 
		expect {
			"*sword*" {
				send_user "ErrExpt:Incorrect Username or Password";
				exit 
			}
			"*]$ " { 
				send "sudo su\r"
			}
		}
	} 
	"*]$ " { 
		send "sudo su\r"
	}
}
expect {
	"*\: " { 
		send "$UPASS\r" 
	}
}
expect {
	"*]$ " {
		send_user "ErrExpt:Sudo privilage denied"
		exit 1
	}
	"*\# " {
		send "pwd\r"
	}
}
expect "*\# " { 
	send_user "HOSTNAME_BEGIN"; 
	send {hostname}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTNAME_END"; 
	send_user "HOSTCPU_BEGIN"; 
	send {lscpu | sed -e 's/[[:space:]]\+/ /g; s/ /_/g; s/:_/: /'}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTCPU_END"; 
	send_user "HOSTDISK_BEGIN"; 
	send {df -kh | sed 's/d o/d_o/;'}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTDISK_END"; 
	send_user "HOSTSYSINF_BEGIN"; 
	send {dmidecode -t 1|sed '1,6d; s/ /_/g; s/:_/: /; s/\\t//'|head -n -1}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTSYSINF_END"; 
	send_user "HOSTDISKSN_BEGIN"; 
	send {lsscsi -s | sed 's/L V/L_V/g;' &&  echo "Name Serial" && lsblk --nodeps -no name,serial}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTDISKSN_END"; 
	send_user "HOSTDNIC_BEGIN"; 
	send {lspci|grep net|grep -v Vir|sed 's/ /_/g; s/:_/: /'}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTDNIC_END"; 
	send_user "HOSTIPMAC_BEGIN"; 
	send {echo Device IP Netmask Broadcast Mac_Address && for DEV in /sys/class/net/*; do a=\`echo \$DEV  | awk -F'/' '{print \$5}'\`; b=\`ifconfig \$a | grep -oE "\\b([0-9]{1,3}\\.){3}[0-9]{1,3}\\b" | tr '\\n' ' '\`; c=\`cat \$DEV/address\`; [ -z "\$b" ] && echo \$a NULL NULL NULL \$c || echo \$a \$b \$c ; done }; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTIPMAC_END"; 
	send_user "HOSTRAM_BEGIN"; 
	send {dmidecode -t memory | grep -iE "Size|Speed:|Part"|grep -v  -iE "No|clock"|awk '{printf "%s%s",\$0,(NR%3?FS:RS)}'|sed 's/ //g; s/\\t/ /'}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTRAM_END"; 
	send_user "HOSTPWS_BEGIN"; 
	send {dmidecode -t 39|sed '1,4d; s/ *$//; s/ /_/g; s/:_/: /; s/\\t//'|grep -v "Handle\|Supply"}; 
	send "\r" 
}
expect "*\# " { 
	send_user "HOSTPWS_END"; 
	send_user "VLIST_BEGIN"; 
	send {virsh list --all|sed '2d'|head -n -1}; 
	send "\r" 
}
expect "*\# " { 
	send_user "VLIST_END"; 
	send "exit\r" 
}
expect "*]$ "
send "exit\r"
expect "out"
send_user "BYE"
exit
EOF
)

ERREXPT=$(echo "$HOSTRESULT" | grep ErrExpt)
if [ ! -z "$ERREXPT" ]
then
	echo "$ERREXPT"
	exit 1
else

echo "Preparing data analysis"
HOSTNAM=$(echo "$HOSTRESULT" | awk /HOSTNAME_BEGIN/,/HOSTNAME_END/ | sed '1d' | head -n -1)
HOSTCPU=$(echo "$HOSTRESULT" | awk /HOSTCPU_BEGIN/,/HOSTCPU_END/ | sed '1d' | head -n -1)
HOSTDSK=$(echo "$HOSTRESULT" | awk /HOSTDISK_BEGIN/,/HOSTDISK_END/ | sed '1d' | head -n -1)
HOSTDSKSN=$(echo "$HOSTRESULT" | awk /HOSTDISKSN_BEGIN/,/HOSTDISKSN_END/ | sed '1d' | head -n -1)
HOSTSYSINF=$(echo "$HOSTRESULT" | awk /HOSTSYSINF_BEGIN/,/HOSTSYSINF_END/ | sed '1d' | head -n -1)
HOSTDNIC=$(echo "$HOSTRESULT" | awk /HOSTDNIC_BEGIN/,/HOSTDNIC_END/ | sed '1d' | head -n -1)
HOSTIPMAC=$(echo "$HOSTRESULT" | awk /HOSTIPMAC_BEGIN/,/HOSTIPMAC_END/ | sed '1d' | head -n -1)
HOSTRAM=$(echo "$HOSTRESULT" | awk /HOSTRAM_BEGIN/,/HOSTRAM_END/ | sed '1d' | head -n -1)
HOSTPWS=$(echo "$HOSTRESULT" | awk /HOSTPWS_BEGIN/,/HOSTPWS_END/ | sed '1d' | head -n -1)
VLIST=$(echo "$HOSTRESULT" | awk /VLIST_BEGIN/,/VLIST_END/ | sed '1d' | head -n -1)

echo "Applying HTML tags"
HOSTHTMLNAM=$(echo "$HOSTNAM" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLCPU=$(echo "$HOSTCPU" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLNDSK=$(echo "$HOSTDSK" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLNDSKSN=$(echo "$HOSTDSKSN" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLSYSINF=$(echo "$HOSTSYSINF" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLDNIC=$(echo "$HOSTDNIC" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLIPMAC=$(echo "$HOSTIPMAC" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLRAM=$(echo "$HOSTRAM" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLPWS=$(echo "$HOSTPWS" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
HOSTHTMLVLIST=$(echo "$VLIST" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
###
HOSTNAM=$(echo $HOSTNAM | sed 's/\r//g')
###
echo "Publishing information in wiki"
HTMLBODY='<ac:layout>
    <ac:layout-section ac:type=\ "single\">
        <ac:layout-cell>
            <p>
                <ac:structured-macro ac:name=\ "toc\" ac:schema-version=\ "1\" ac:macro-id=\ "18b70996-d169-4c6c-8ece-4a72e26ae068\" />
            </p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "two_equal\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Product Details</span></strong></h1>
            <p><strong>Storage Block Information</strong><br/>'"${HOSTHTMLSYSINF}"'<br/></p>
        </ac:layout-cell>
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Hardware Information</span></strong></h1>
            <p><strong>CPU Information</strong><br/>'"${HOSTHTMLCPU}"'<br/></p>
            <p><strong>RAM Information</strong><br/>'"${HOSTHTMLRAM}"'<br/></p>
            <p><strong>Storage Block Information</strong><br/>'"${HOSTHTMLNDSKSN}"'<br/></p>
            <p><strong>Power Information</strong><br/>'"${HOSTHTMLPWS}"'<br/></p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "single\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Management Information</span></strong></h1>
            <p><strong>HOSTNAME</strong><br/>'"${HOSTHTMLNAM}"'<br/></p>
            <p><strong>IP and MAC Information</strong><br/>'"${HOSTHTMLIPMAC}"'<br/></p>
            <p><strong>Network Device Information</strong><br/>'"${HOSTHTMLDNIC}"'<br/></p>
            <p><strong>Disk Usage Information</strong><br/>'"${HOSTHTMLNDSK}"'<br/></p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "single\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Virtual Machine Information</span></strong></h1>
            <p><strong>Virtual Machine Information</strong><br/>'"${HOSTHTMLVLIST}"'<br/></p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "single\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Configuration&nbsp;</span></strong><span style=\"color: rgb(1,87,162);\"><strong>Management</strong></span></h1>
            <p></p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "two_equal\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Troubleshooting</span></strong></h1>
            <p></p>
        </ac:layout-cell>
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Reference</span></strong></h1>
            <p></p>
        </ac:layout-cell>
    </ac:layout-section>
</ac:layout>'

BODY="$(echo "$HTMLBODY"|sed 's/^[ \t]*//; s#\\ \"#\\\"#g'|tr -d '\n')"
HOSTPAGERESULT=$(
curl -k -u $WIKIUSR:$WIKIPASS -X POST $WIKIURL \
-H 'Content-Type: application/json' \
-d '{"type":"page","title":"'"$HOSTNAM"'","ancestors":[{"id":"'"${PARENTPAGE}"'"}], "space":{"key":"'"${SPACE}"'"},"body":{"storage":{"value":"'"${BODY}"'","representation":"storage"}}}'  | jq "."
)

HOSTRESPONSECODE=$(echo $HOSTPAGERESULT | jq '.statusCode' | sed 's/\r//')
HOSTRESPONSEMESSAGE=$(echo $HOSTPAGERESULT | jq '.message' | sed 's/"//g; s/\r//')

	if [ $HOSTRESPONSECODE = "null" ]; 
	then

		echo "Wiki page created $HOSTNAM"
		HOSTPAGEID=$(echo $HOSTPAGERESULT | jq '.id' | sed 's/"//g; s/\r//')
		HOSTPAGETITLE=$(echo $HOSTPAGERESULT | jq '.title' | sed 's/"//g')
		HOSTPAGEBODY=$(echo $HOSTPAGERESULT | jq '.body.storage.value' | sed 's/\r//')
		HOSTPAGEBASE=$(echo $HOSTPAGERESULT | jq '._links.base' | sed 's/"//g; s/\r//')
		HOSTPAGEUI=$(echo $HOSTPAGERESULT | jq '._links.webui' | sed 's/"//g; s/\r//')
		echo -e "URL: ""\e[4;31;47m$HOSTPAGEBASE\e[0m""\e[4;31;47m$HOSTPAGEUI\e[0m"

		echo "Extracting guest machine data"
		VMNAME=$(echo "$VLIST" | grep -i running | awk '{print $2}')
#		VMNAME="pennytel01-cms01-n03-gsy1.srv.symbionetworks.com"
#		VMNAME="pennytel01-crm01-n03-gsy1.srv.symbionetworks.com"

		if [ -z "$VMNAME" ]; 
		then
			echo "No Virtual Machine is currently running"
			echo "Execution Complete"
			exit
		else
			echo "$VMNAME" |  (while read line
				do
RESULT=$(
/usr/bin/expect << EOF
						log_file myfile.log
						#log_user 0
						set timeout 30
						spawn ssh -o StrictHostKeyChecking=no $UNAME@$HOSTIP
						expect {
							 timeout {
							 	send_user "ErrExpt:Connection timed out"
								exit 1
							}
							"Connection refused" {
								send_user "ErrExpt:Connection refused"
								exit 1
							}
							"*sword*" { 
								send "$UPASS\r" 
								expect {
									"*sword*" {
										send_user "ErrExpt:Incorrect Username or Password";
										exit 
									}
									"*]$ " { 
										send "sudo su\r"
									}
								}
							} 
							"*]$ " { 
								send "sudo su\r"
							}
						}
						expect {
							"*\: " { 
								send "$UPASS\r" 
							}
						}
						expect {
							"*]$ " {
								send_user "ErrExpt:Sudo privilage denied"
								exit 1
							}
							"*\# " {
								send "pwd\r"
							}
						}
						expect {
							"*\# " { 
								send_user "DOMINFO_BEGIN\n"; 
								send {virsh dominfo $line|grep -e 'Id\|Name\|State\|CPU(s)\|Max memory\|Persistent\|Autostart'|awk -F':' '\$1 ~ /Max memory/ { print \$1":\\t"\$2/1024/1024"GB";next }1' | sed 's/x m/x_M/'}; 
								send "\r" 
							}
						}
						expect {
							"*\# " { 
								send_user "DOMINFO_END\n"; 
								send_user "BLOCK_BEGIN\n"; 
								send {virsh domstats $line | awk -F'=' '/block\\.[0-9]\\.physical|block\\.[0-9]\.name|block\\.[0-9]\\.path/ { if (\$2 ~ /^[:0-9:]/) print \$1"="\$2/1024/1024/1024"GB"; else print \$0;}' | sed 's/=/: /' | column -t}; 
								send "\r" 
							}
						}
						expect { 
							"*\# " {
							send_user "BLOCK_END\n"; 
							send_user "VCPU_BEGIN\n";  
							send "virsh vcpupin $line | sed '2d; 1s/ /_/2g'\r" 
							}
						}
						expect { 
							"*\# " {
							send_user "VCPU_END\n"; 
							send_user "RELEASE_BEGIN\n"; 
							send "virt-cat -d $line /etc/redhat-release | sed 's/ /_/g'\r" 
							}
						}
						expect { 
							"*\# " {
							send_user "RELEASE_END\n"; 
							send_user "DISKUSAGE_BEGIN\n"; 
							send "guestfish -d $line --ro -i df-h\r" 
							}
						}
						expect { 
							"*\# " {
							send_user "DISKUSAGE_END\n"; 
							send_user "NETWORK_BEGIN\n"; 
							send {virt-ls -d $line /etc/sysconfig/network-scripts/ | grep ifcfg | grep -v lo | while read line; do virt-cat -d $line /etc/sysconfig/network-scripts/\$line|grep 'DEVICE\|HWADDR\|MASTER\|IPADDR\|NETMASK\|GATEWAY'|sed 's/=/: /'|column -t; echo " ";done};
							send "\r"
							}
						}
						expect { 
							"*\# " {
							send_user "NETWORK_END\n";
							send_user "IRQ_BEGIN\n";
							send {virsh dumpxml $line|grep "address domain"|awk -F"'" '{print substr(\$2,3,6)"."substr(\$4,3,6)"."substr(\$6,3,6)"."substr(\$8,3,6)}'|while read line; do irq=\`grep "\$line" /proc/interrupts |cut -d' ' -f2 |sed 's/://'\`; echo "PCIBUS: "\$line "IRQ: "\$irq| column -t; done}; 
							send "\r" 
							}
						}
						sleep 5;
						expect { 
							"*\# " {
							send_user "IRQ_END\n";
							send_user "SMP_BEGIN\n"; 
							send {grep "\$(virsh dumpxml $line|grep "address domain"|awk -F"'" '{print substr(\$2,3,6)"."substr(\$4,3,6)"."substr(\$6,3,6)"."substr(\$8,3,6)}')" /proc/interrupts |cut -d' ' -f2 |sed 's/://'|while read line; do hexcpu=\`cat /proc/irq/\$line/smp_affinity|sed 's/,//'\`; hexlen=\`cat /proc/irq/\$line/smp_affinity|sed 's/,//'|awk '{print length}'\`; bincpu=" "; for (( c=0; c<\$hexlen; c++ )); do bincpu+=\`perl -e 'printf "%04b\n",'\${hexcpu:\$c:1}\`; bincpu+=" "; done ; echo "\$line \$bincpu"|column -t; done}; 
							send "\r"
							}
						}
						expect { 
							"*\# " {
							send_user "SMP_END\n"; 
							send "exit\r" 
							}
						}
						expect {
							"*\$ " {
							send "exit\r" 
							}
						}
						expect "out"
							send_user "BYE"
							exit
EOF
)
echo "$RESULT" | sed 's/\^M//g'> dam
echo "Preparing data analysis"
VMINFO=$(echo "$RESULT" | awk /DOMINFO_BEGIN/,/DOMINFO_END/ | sed '1,2d' | head -n -1)
VMBLK=$(echo "$RESULT" | awk /BLOCK_BEGIN/,/BLOCK_END/ | sed '1,2d' | head -n -1)
VCPU=$(echo "$RESULT" | awk /VCPU_BEGIN/,/VCPU_END/ | sed '1,2d' | head -n -2)
OSVERSION=$(echo "$RESULT" | awk /RELEASE_BEGIN/,/RELEASE_END/ | sed '1,2d' | head -n -1)
DISKUSAGE=$(echo "$RESULT" | awk /DISKUSAGE_BEGIN/,/DISKUSAGE_END/ | sed '1,4d; s/d o/d_o/' | head -n -2)
NETWORK=$(echo "$RESULT" | awk /NETWORK_BEGIN/,/NETWORK_END/ | head -n -2 | sed '1,2d; s/\"//g;')
IRQ=$(echo "$RESULT" | awk /IRQ_BEGIN/,/IRQ_END/ | head -n -1 | sed '1,2d')
SMP=$(echo "$RESULT" | awk /SMP_BEGIN/,/SMP_END/ | head -n -1 | sed '1,2d')

echo ---------
echo "$VMINFO"
echo ----------
echo "$VMBLK"
echo -------------
echo "$VCPU"
echo ----------
echo "$OSVERSION"
echo -----------
echo "$DISKUSAGE"
echo -------------
echo "$NETWORK"
echo ---------------
echo "$IRQ"
echo ---------------
echo "$SMP"
echo -------------

echo "Applying HTML tags"
VMHOSTHTMLNAM=$(echo "$line" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLINFO=$(echo "$VMINFO" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLBLK=$(echo "$VMBLK" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLVCPU=$(echo "$VCPU" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLVER=$(echo "$OSVERSION" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLDSK=$(echo "$DISKUSAGE" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLNET=$(echo "$NETWORK" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLIRQ=$(echo "$IRQ" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')
VMHTMLSMP=$(echo "$SMP" | awk 'BEGIN{print "<table>"} {print "<tr>";for(i=1;i<=NF;i++)print "<td>" $i"</td>";print "</tr>"} END{print "</table>"}' | tr '\n' ' ' | sed 's/ //g')

#echo "$VMHTMLINFO"
#echo "$VMHTMLBLK"
#echo "$VMHTMLVCPU"
#echo "$VMHTMLVER"
#echo "$VMHTMLDSK"
#echo "$VMHTMLNET"
#echo "$VMHTMLIRQ"
#echo "$VMHTMLSMP"


echo "Publishing information in wiki"
VMHTMLBODY='<ac:layout>
    <ac:layout-section ac:type=\ "single\">
        <ac:layout-cell>
            <p>
                <ac:structured-macro ac:name=\ "toc\" ac:schema-version=\ "1\" ac:macro-id=\ "18b70996-d169-4c6c-8ece-4a72e26ae068\" />
            </p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "two_equal\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">VM Information</span></strong></h1>
            <p><strong>Virtual Machine Info</strong><br/>'"${VMHTMLINFO}"'<br/></p>
            <p><strong>Operating System</strong><br/>'"${VMHTMLVER}"'<br/></p>
            <p><strong>Disk Info</strong><br/>'"${VMHTMLDSK}"'<br/></p>

        </ac:layout-cell>
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Hardware Information</span></strong></h1>
            <p><strong>Virtual CPU Affinity</strong><br/>'"${VMHTMLVCPU}"'<br/></p>
            <p><strong>Block Device Info</strong><br/>'"${VMHTMLBLK}"'<br/></p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "single\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Management Information</span></strong></h1>
            <p><strong>HOSTNAME</strong><br/>'"${VMHOSTHTMLNAM}"'<br/></p>
            <p><strong>Network Info</strong><br/>'"${VMHTMLNET}"'<br/></p>
            <p><strong>Ethernet IRQ ID</strong><br/>'"${VMHTMLIRQ}"'<br/></p>
            <p><strong>IRQ assigned CPU</strong><br/>'"${VMHTMLSMP}"'<br/></p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "single\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Configuration&nbsp;</span></strong><span style=\"color: rgb(1,87,162);\"><strong>Management</strong></span></h1>
            <p></p>
        </ac:layout-cell>
    </ac:layout-section>
    <ac:layout-section ac:type=\ "two_equal\">
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Troubleshooting</span></strong></h1>
            <p></p>
        </ac:layout-cell>
        <ac:layout-cell>
            <h1><strong><span style=\"color: rgb(1,87,162);\">Reference</span></strong></h1>
            <p></p>
        </ac:layout-cell>
    </ac:layout-section>
</ac:layout>'

BODY="$(echo "$VMHTMLBODY"|sed 's/^[ \t]*//; s#\\ \"#\\\"#g'|tr -d '\n')"
#Create VM Page
#VMHTMLBODY=$(echo "<strong>Virtual Machine Info</strong><br/>$VMHTMLINFO<br/>\
#<strong>Block Device Info</strong><br/>$VMHTMLBLK<br/>\
#<strong>Virtual CPU Affinity</strong><br/>$VMHTMLVCPU<br/>\
#<strong>Operating System</strong><br/>$VMHTMLVER<br/>\
#<strong>Disk Info</strong><br/>$VMHTMLDSK<br/>\
#<strong>Network Info</strong><br/>$VMHTMLNET<br/>\
#<strong>Ethernet IRQ ID</strong><br/>$VMHTMLIRQ<br/>\
#<strong>IRQ assigned CPU</strong><br/>$VMHTMLSMP<br/>")
#VMPAGERESULT=$(curl -k -u $WIKIUSR:$WIKIPASS -X POST $WIKIURL -H 'Content-Type: application/json' -d '{"type":"page","title":"'"$line"'","ancestors":[{"id":"'"${HOSTPAGEID}"'"}],"space":{"key":"'"${SPACE}"'"},"body":{"storage":{"value":"'"${VMHTMLBODY}"'","representation":"storage"}}}'  | jq ".")

VMPAGERESULT=$(
curl -k -u $WIKIUSR:$WIKIPASS -X POST $WIKIURL \
-H 'Content-Type: application/json' \
-d '{"type":"page","title":"'"$line"'","ancestors":[{"id":"'"${HOSTPAGEID}"'"}],"space":{"key":"'"${SPACE}"'"},"body":{"storage":{"value":"'"${BODY}"'","representation":"storage"}}}' | jq "."
)

VMRESPONSECODE=$(echo $VMPAGERESULT | jq '.statusCode' | sed 's/\r//')
VMRESPONSEMESSAGE=$(echo $VMPAGERESULT | jq '.message' | sed 's/"//g; s/\r//')


			if [ $VMRESPONSECODE = "null" ]; 
			then
				echo "Wiki page created $line"
				VMPAGEID=$(echo $VMPAGERESULT | jq '.id' | sed 's/"//g; s/\r//')
				VMPAGETITLE=$(echo $VMPAGERESULT | jq '.title' | sed 's/"//g; s/\r//g')
				VMPAGEBODY=$(echo $VMPAGERESULT | jq '.body.storage.value' | sed 's/\r//g')
				VMPAGEBASE=$(echo $VMPAGERESULT | jq '._links.base' | sed 's/"//g; s/\r//g')
				VMPAGEUI=$(echo $VMPAGERESULT | jq '._links.webui' |sed 's/"//g; s/\r//g')
				echo $VMPAGERESULT | jq '._links.base' | sed 's/"//g; s/\r//g'
				echo -e "URL: ""\e[4;31;47m$VMPAGEBASE\e[0m""\e[4;31;47m$VMPAGEUI\e[0m"
			else
				echo "$VMRESPONSECODE"
				echo "$VMRESPONSEMESSAGE"
				exit 1
			fi
				done
				)

		fi

	else
			echo "$HOSTRESPONSECODE"
			echo "$HOSTRESPONSEMESSAGE"
	fi

fi
echo "Execution Complete"